l = [ int(n) for n in open('day2020_01.txt') ]

def find(v):
    seen = set()
    for k in l:
        if v-k in seen:
            return (k, v-k)
            break
        seen.add(k)
    return None

for e in l:
    r = find(2020-e)
    if r is not None:
        print(e,r[0],r[1],e*r[0]*r[1])
        break


