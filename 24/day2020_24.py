from collections import defaultdict

tiling = defaultdict(bool)

for l in open('day2020_24.txt'):
    l = l.strip()
    for a,b in [ ("se","S"), ("sw","s"), ("ne", "N"), ("nw", "n") ]:
        l = l.replace(a,b)
    pos = [0,0]
    for d in l:
        if d == "e":
            pos[0] += 1
        if d == "w":
            pos[0] -= 1
        if d == "S":
            pos[0] += 0.5
            pos[1] -= 1
        if d == "s":
            pos[0] -= 0.5
            pos[1] -= 1
        if d == "N":
            pos[0] += 0.5
            pos[1] += 1
        if d == "n":
            pos[0] -= 0.5
            pos[1] += 1
    tiling[tuple(pos)] = not tiling[tuple(pos)]

print('Part 1', sum(1 for v in tiling.values() if v))

for rule in range(100):
    new_tiling = defaultdict(bool)

    to_explore = []
    for x,y in tiling:
        if tiling[x,y]:
            neighbours = [ (x-1,y), (x+1,y), (x-0.5,y+1), (x+0.5,y+1),
                    (x-0.5,y-1), (x+0.5,y-1) ]
            for v in [(x,y)] + neighbours:
                if v not in to_explore:
                    to_explore.append(v)
    for x, y in to_explore:
        neighbours = [ (x-1,y), (x+1,y), (x-0.5,y+1), (x+0.5,y+1),
                (x-0.5,y-1), (x+0.5,y-1) ]
        nblack = sum(1 for v in neighbours if tiling[v])
        if tiling[x,y] and nblack in [1,2]:
            new_tiling[x,y] = True
        if not tiling[x,y] and nblack == 2:
            new_tiling[x,y] = True

    tiling = new_tiling
    print('Day', 1+rule, sum(1 for v in tiling.values() if v))

