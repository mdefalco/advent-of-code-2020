
d = { i:i for i in range(10) }

for v in sorted(d, key=lambda v: v):
    print(v, d[v])
    for ov in d:
        if ov % 2 == 0:
            d[ov] -= 1
