import re
import math
from itertools import chain

rules = []
rule_names = []
lines = (l.strip() for l in open('day2020_16.txt'))

while True:
    l = next(lines)
    if l == '': break
    rules.append([ 
        [ int(n) for n in v.split('-') ] \
            for v in l[l.index(': ')+2:].split(' or ') ])
    rule_names.append(l[:l.index(': ')])

next(lines) # your ticket
ticket = list(map(int, next(lines).split(',')))

next(lines); next(lines) # nearby tickets
other_tickets = [ list(map(int, line.split(','))) for line in lines ]

valid = 0

print('Part 1', sum(value for value in chain.from_iterable(other_tickets)
    if not any( r[0] <= value <= r[1] for r in chain.from_iterable(rules) ) ))


valid_tickets = tuple( ticket for ticket in other_tickets 
    if not any(not any( r[0] <= value <= r[1] for r in chain.from_iterable(rules) )
        for value in ticket ))

candidates = { 
    rule_names[i] : list( j for j in range(len(ticket))
            if all( r[0][0] <= t[j] <= r[0][1] or r[1][0] <= t[j] <= r[1][1]
                for t in valid_tickets ) )
            for i, r in enumerate(rules)
            }

position = {}
def re_sorted(iterable, key=lambda v:v):
    data = list(iterable)
    while data:
        v = data[0]
        for ov in data[1:]:
            if key(ov) < key(v):
                v = ov
        data.remove(v)
        yield v

for rulename in e_sorted(candidates, key=lambda r: len(candidates[r])):
    i = candidates[rulename][0]
    position[rulename] = i
    for other_rulename in candidates:
        try:
            candidates[other_rulename].remove(i)
        except ValueError:
            continue

print('Part 2', math.prod(ticket[position[r]] for r in position 
    if r.startswith('departure')))
