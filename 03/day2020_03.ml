let w = 31
let h = 323

let _ =
    let ic = open_in "day2020_03.txt" in
    (* Assez pénible avec des listes, mais c'est le seul
    type de donnée pour le moment avec OCaml dans les TPs *) 
    (* Ici map sera une liste de lignes et chaque ligne est une liste
       de caractères *)
    let map = List.init h
        (fun _ -> 
            let l = List.init w (fun _ -> input_char ic) in
            assert (input_char ic == '\n');
            l) in
    (* Juste pour vérifier qu'on l'a bien lu *)
    List.iter
        (fun l -> Printf.printf "%s\n" (String.concat "" (List.map (fun c -> String.make 1 c) l)))
        map

    
