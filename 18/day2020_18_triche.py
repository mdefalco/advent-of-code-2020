import ast

swap_p_m = lambda s: s.replace('*', 'OpT').replace('+','*').replace('OpT','+')

class Swapper(ast.NodeTransformer):
    def visit_Add(self, node):
        return ast.Mult()
    def visit_Mult(self, node):
        return ast.Add()

s = 0
for l in open('day2020_18.txt'):
    t = ast.parse(swap_p_m(l.strip()), mode='eval')
    t2 = Swapper().visit(t)
    v = eval(compile(t2,'',mode='eval'))
    s += v
print('Part 2', s)
