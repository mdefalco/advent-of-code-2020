from more_itertools import peekable

def parse_ope(g):
    v = next(g)
    if v in [ str(n) for n in range(10) ]:
        return int(v)
    if v == '(':
        expr = parse_expr(g)
        next(g) # eat )
        return expr
    raise SyntaxError

def parse_expr(g):
    e = parse_ope(g)
    while g.peek('') in ['*','+']:
        op = next(g)
        ope = parse_ope(g)
        e = (e, op, ope)
    return e

def eval_expr(expr):
    if type(expr) == int:
        return expr
    ope1, op, ope2 = expr
    d_op = { '+' : lambda x, y : x + y,
            '*' : lambda x, y : x * y }
    return d_op[op](eval_expr(ope1), eval_expr(ope2))
    
s = 0
for l in open('day2020_18.txt'):
    g = peekable(l.strip().replace(' ',''))
    e = parse_expr(g)
    s += eval_expr(e)
print('Part 1', s)
