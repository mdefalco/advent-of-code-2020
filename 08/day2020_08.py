import re

r = re.compile(r'''(?P<ope>[a-z][a-z][a-z]) (?P<arg>(\+|-)\d+)''')

instructions = [ (m.group('ope'), int(m.group('arg'))) for m in \
        map(r.search, open('day2020_08.txt')) ]

nop_jmps = [ i for i, instr in enumerate(instructions) if instr[0] != 'acc' ]

def terminates():
    ran = []
    pc, acc = 0, 0
    while pc not in ran:
        if pc >= len(instructions):
            return True, acc
        ran.append(pc)
        ope, arg = instructions[pc]
        pc += 1
        if ope == 'jmp': pc += arg-1
        if ope == 'acc': acc += arg
    return False, acc

for i in nop_jmps:
    ope, arg = instructions[i]
    nope = 'nop' if ope == 'jmp' else 'jmp'
    instructions[i] = (nope, arg)
    t, v = terminates()
    if t:
        print(v)
    instructions[i] = (ope, arg)

