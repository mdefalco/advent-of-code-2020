import re

lines = iter(open('day2020_19.txt'))

rules = {}

while True:
    l = next(lines).strip()
    if l == '': break

    num, rule = l.split(': ')
    
    if '"' in rule:
        rule = rule.strip(' "')
    else:
        rule = [ choice.split() for choice in rule.split('|') ]

    rules[num] = rule

def rules2re(n):
    rule = rules[n]
    if type(rule) == str:
        return rule
    choice2re = lambda p: ''.join(map(rules2re, p))
    if len(rule) == 1:
        return choice2re(rule[0])
    return '(' + '|'.join(choice2re(p) for p in rule) + ')'

r = re.compile(rules2re('0'))

print(sum(1 for l in lines if r.fullmatch(l.strip())))

def rules2re(n):
    rule = rules[n]
    if type(rule) == str:
        return rule
    choice2re = lambda p: ''.join(map(rules2re, p))
    if len(rule) == 1:
        return choice2re(rule[0])
    return '(' + '|'.join(choice2re(p) for p in rule) + ')'

r = re.compile(rules2re_looping('0'))

print(sum(1 for l in lines if r.fullmatch(l.strip())))
