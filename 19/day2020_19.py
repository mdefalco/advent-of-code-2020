import re

lines = iter(open('day2020_19.txt'))

rules = {}

while True:
    l = next(lines).strip()
    if l == '': break

    num, rule = l.split(': ')
    
    if '"' in rule:
        rule = rule.strip(' "')
    else:
        rule = [ choice.split() for choice in rule.split('|') ]

    rules[num] = rule

lines = tuple(lines)

def match_prod(prod, line):
    for r in prod:
        line = match_rule(r, line)
        if line is None: break
    return line

def match_rule(rn, line):
    rule = rules[rn]
    if type(rule) == str:
        if line != '' and line[0] == rule: return line[1:]
        return None

    for prod in rule:
        match = match_prod(prod, line)
        if match is not None: return match

    return None

def valid(line):
    return match_rule('0',line) == ''

print('Part 1', sum(1 for line in lines if valid(line.strip())))

def valid_loop(line):
    matched_42 = 0
    while True:
        line = match_rule('42',line)
        if line is None: return False
        matched_42 += 1

        nline = line
        for i in range(1,matched_42):
            nline = match_rule('31',nline)
            if nline is None:
                break
            if nline == '':
                return True
    # unreachable

print('Part 2', sum(1 for line in lines if valid_loop(line.strip())))
