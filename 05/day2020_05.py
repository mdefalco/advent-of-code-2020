

def dicho(path, init=(0,127)):
    r = list(init)
    for v in path:
        m = (r[0]+r[1])//2
        if v in ['B','R']:
            r[0] = m+1
        else:
            r[1] = m
    return r[0]

def col(path): return dicho(path, (0,7))
def row(path): return dicho(path, (0,127))

def seatid(path):
    r, c = row(path[:-3]), col(path[-3:])
    return r * 8 + c

print('Part 1', max(seatid(v.strip()) for v in open('day2020_05.txt')))

seatids = [ seatid(v.strip()) for v in open('day2020_05.txt') ]
seatids.sort()
for v in range(seatids[0],seatids[-1]):
    if v-1 in seatids and v+1 in seatids and v not in seatids:
        print(v)
