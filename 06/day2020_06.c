#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int main()
{
    FILE *fp = fopen("day2020_06.txt", "r"); 
    if (!fp) return 1;

    int count_any = 0;
    int count_all = 0;

    while(!feof(fp))
    {
        int any[128] = {};

        int nanswer = 0;
        char answer[200];

        while(fgets(answer, 200, fp))
        {
            if (answer[0] == '\n') // dernière réponse
                break;

            int i = 0;
            while(answer[i] != '\n')
            {
                any[answer[i]]++;
                i++;
            }
            nanswer++;
        }

        int l_count_any = 0;
        int l_count_all = 0;
        for (int i = 0; i < 128; i++)
        {
            if (any[i] > 0) count_any++;
            if (any[i] == nanswer) count_all++;
        }
    }

    printf("Part 1: %d\n", count_any);
    printf("Part 2: %d\n", count_all);

    fclose(fp);

    return 0;
}
