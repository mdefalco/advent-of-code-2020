
l = iter(open('day2020_06.txt'))

groups = []

group = []
try:
    while True:
        v = next(l).strip()
        if v == '':
            groups.append(group)
            group = []
        else:
            group.append(v)
except StopIteration:
    groups.append(group)

r = 0
r2 = 0
for group in groups:
    questions = set()
    for v in group:
        for q in v:
            questions.add(q)
    all_yes = { q : True for q in questions }
    for v in group:
        for q in questions:
            if q not in v:
                all_yes[q] = False
    r2 += sum(1 for q in all_yes if all_yes[q])
    r += len(questions)

print('Part 1', r)
print('Part 2', r2)
