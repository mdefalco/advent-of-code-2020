
raw_lines = [ l.strip()[:-1].split(' (contains ') for l in open('day2020_21.txt') ]

def intersection(l1, l2):
    return [ e for e in l2 if e in l1 ]

food_allergen = {}
allergen_food = {}
food_occ = {}
for a,b in raw_lines:
    foods = a.split()
    allergens = b.split(', ')
    for food in foods:
        food_occ[food] = 1 + food_occ.get(food, 0)
    for allergen in allergens:
        if allergen not in allergen_food:
            allergen_food[allergen] = foods
        else:
            allergen_food[allergen] = intersection(foods,
                    allergen_food[allergen])

allergens = list(allergen_food.keys())

while allergens != []:
    for allergen in allergens:
        if len(allergen_food[allergen]) == 1:
            food = allergen_food[allergen][0]
            allergens.remove(allergen)
            food_allergen[food] = allergen
            for other_allergen in allergens:
                if food in allergen_food[other_allergen]:
                    allergen_food[other_allergen].remove(food)
            break
    
print('Part 1', sum(o for food, o in food_occ.items() 
    if food not in food_allergen))

print('Part 2', ','.join(sorted(food_allergen.keys(),key=lambda f: food_allergen[f])))
