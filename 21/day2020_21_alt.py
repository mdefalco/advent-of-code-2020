
raw_lines = [ l.strip()[:-1].split(' (contains ') for l in open('day2020_21_ex.txt') ]


assoc = [ (a.split(), b.split(', ')) for (a,b) in raw_lines ]

allergens = []
foods = []

for food_l, all_l in assoc:
    for food in food_l:
        if food not in foods: foods.append(food)
    for allergen in all_l:
        if allergen not in allergens: allergens.append(allergen)

foods.sort()
allergens.sort()

M = [ [ 0 ] * len(allergens) for _ in foods ]

for food_l, all_l in assoc:
    for food in food_l:
        for allergen in all_l:
            M[foods.index(food)][allergens.index(allergen)] = 1

for l in M:
    print(l)
