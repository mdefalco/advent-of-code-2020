p = 25
l = [ int(n) for n in open('day2020_09.txt') ]
sommes = {}
preamble = set(l[:p])
for i, v in enumerate(l[p:]):
    is_sum = False
    for k in preamble:
        if v-k != k and v-k in preamble:
            is_sum = True
            break
    if not is_sum:
        break
    preamble.remove(l[i])
    preamble.add(v)

to_find = v

current_sum = l[0]+l[1]
start = 0
end = 1

for i in range(len(l)):
    if current_sum > to_find:
        current_sum -= l[start]
        start += 1
    elif current_sum < to_find:
        end += 1
        current_sum += l[end]
    else:
        break

plage = l[start:end+1]

print(min(plage)+max(plage))
