
let adapters = [|17;110;146;144;70;57;124;121;134;12;135;120;19;92;6;103;46;56;93;65;14;31;63;41;131;60;73;83;71;37;85;79;13;7;109;24;94;2;30;3;27;77;91;106;123;128;35;26;112;55;97;21;100;88;113;117;25;82;129;66;11;116;64;78;38;99;130;84;98;72;50;36;54;8;34;20;127;1;137;143;76;69;111;136;53;43;140;145;49;122;18;42|]

let part1 () =
    Array.sort compare adapters;
    let count1 = ref 0 in
    let count3 = ref 1 in (* last one is +3 *)
    for i = 0 to Array.length adapters - 1 do
        let previous = if i = 0 then 0 else adapters.(i-1) in
        let diff = adapters.(i) - previous in
        if diff = 1 then incr count1;
        if diff = 3 then incr count3
    done;
    Printf.printf "Count 1 : %d, Count 3 : %d\n" !count1 !count3;
    Printf.printf "Answer: %d\n" (!count1 * !count3)


let part2 () =
    Array.sort compare adapters;
    let n = Array.length adapters in
    let cache = Array.make (n+1) None in
    let rec count prev =
        match cache.(prev) with
        | None ->
            let c = ref 0 in
            let next, vprev = if prev = n then 0, 0 else prev+1,adapters.(prev) in
            let leaf = ref true in
            for i = next to min (n-1) (next+2) do
                let v = adapters.(i) in
                if v - vprev <= 3
                then begin
                    c := !c + count i;
                    leaf := false
                end
            done;
            let v = if !leaf then 1 else !c in
            cache.(prev) <- Some v;
            v
        | Some v -> v
    in 
    Printf.printf "%d\n" (count n)

let _ =
    part2 ()
