from itertools import product, chain
from math import sqrt

lines = list(open('day2020_20.txt'))
tiles = { int(lines[12*k][5:-2]) : [ list(lines[12*k+i].strip()) for i in range(1,11) ] 
    for k in range(len(lines)//12) }
N = int(sqrt(len(tiles)))

TOP, RIGHT, BOTTOM, LEFT = 0, 1, 2, 3
def border_dir(direction, f_tile):
    dir_func = [ lambda i: (i,0), lambda i: (9,i), lambda i: (i,9), lambda i: (0,i) ]
    return [ f_tile(*dir_func[direction](i)) for i in range(10) ]

def flip_and_rotate(x,y,dim,flip,rotation):
    if flip:
        x, y = dim-1-x, y
    rot = lambda x, y: (dim-1-y,x)
    for _ in range(rotation):
        x, y = rot(x, y)
    return x, y

def border(direction,tile_id,flip,rotation):
    tile = tiles[tile_id]
    def f_tile(x,y):
        x, y = flip_and_rotate(x,y,10,flip,rotation)
        return tile[y][x]

    return border_dir(direction, f_tile)

def next_pos(x,y):
    if x < N-1: return (x+1,y)
    if y < N-1: return (0,y+1)
    return None

def backtracking(pos, to_tile, tiled={}):
    if to_tile == []: return tiled
    x, y = pos
    for tile_id, flip, rotation in product(to_tile, [True,False], [0,1,2,3]):
        if x > 0 and border(RIGHT, *tiled[x-1,y]) != border(LEFT,tile_id,flip,rotation):
            continue
        if y > 0 and border(BOTTOM, *tiled[x,y-1]) != border(TOP,tile_id,flip,rotation):
            continue
        tiled[x,y] = (tile_id, flip, rotation)
        res = backtracking(next_pos(x,y), 
                [ t for t in to_tile if t != tile_id ], tiled)
        if res is not None: return res
        del tiled[x,y]

tiling = backtracking((0,0), tiles.keys())
print('Part 1', tiling[0,0][0]*tiling[N-1,0][0]*tiling[0,N-1][0]*tiling[N-1,N-1][0])

tile_map = [ [ '.' ] * (8*N) for _ in range(8*N) ]
for i, j in product(range(N), repeat=2):
    tile_id, flip, rotation = tiling[i,j]
    tile = tiles[tile_id]
    for x, y in product(range(8), repeat=2):
        nx, ny = flip_and_rotate(x+1,y+1,10,flip,rotation)
        tile_map[8*j+y][8*i+x] = tile[ny][nx]

text_sea_monster = '''                  # 
#    ##    ##    ###
 #  #  #  #  #  #   '''.split('\n')

sea_monster = []
for j,l in enumerate(text_sea_monster):
    for i in range(len(l)):
        if text_sea_monster[j][i] == '#':
            sea_monster.append((i,j))

seaw, seah = len(text_sea_monster[0]), len(text_sea_monster)

for flip, rotation in product([False,True],range(4)):
    def image(x,y):
        x, y = flip_and_rotate(x, y, 8*N, flip, rotation)
        return tile_map[y][x]
    fr_tile_map =[ [ image(x,y) for x in range(8*N) ] for y in range(8*N) ]
    found_monsters = 0
    for x in range(8*N-seaw):
        for y in range(8*N-seah):
            found = all( fr_tile_map[y+dy][x+dx] == '#' for (dx,dy) in sea_monster )
            if found:
                found_monsters += 1
                for dx, dy in sea_monster:
                    fr_tile_map[y+dy][x+dx] = 'O'
    if found_monsters != 0:
        print('Part 2', sum(1 for s in chain.from_iterable(fr_tile_map) if s == '#'))
        break

if False:
    for flip in [False, True]:
        for rotation in range(4):
            print(flip, rotation)
            v = [ [ ' ' ] * 10 for _ in range(10) ]
            v[0] = border(TOP,1951, flip, rotation)
            v[9] = border(BOTTOM,1951, flip, rotation)
            for i in range(10):
                v[i][9] = border(RIGHT,1951,flip,rotation)[i]
                v[i][0] = border(LEFT,1951,flip,rotation)[i]

            print('\n'.join([ ''.join(l) for l in v ]))

            print()

            v = [ [ ' ' ] * 10 for _ in range(10) ]
            tile = tiles[1951]
            for x, y in product(range(10), repeat=2):
                nx, ny = flip_and_rotate(x,y,10,flip,rotation)
                v[y][x] = tile[ny][nx]
            print('\n'.join([ ''.join(l) for l in v ]))

