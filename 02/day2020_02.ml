let lit_entree ib = 
    Scanf.bscanf ib "%d-%d %c: %s\n"
        (fun i j c password -> (* la fonction a appliquer sur ce qu'on a lu *)
            (i, j, c, password))

let rec lit_tout ib =
    try
        let e = lit_entree ib in (* petite subtilité OCaml évalue de droite
        à gauche donc si on écrit directement lit_entree en dessous ça
        fait une boucle infinie *)
        e :: lit_tout ib (* on lit une entree et on passe a la suite *)
    with End_of_file -> [] (* on arrive ici quand on a lu tout le fichier *)
    
let rec compte_occ s c i =
        if i >= String.length s
        then 0
        else (if s.[i] = c then 1 else 0) + compte_occ s c (i+1)
let valid1 (i, j, c, password) =
    let compte = compte_occ password c 0 in
    compte >= i && compte <= j

let valid2 (i, j, c, password) =
    let ci = password.[i-1] = c in
    let cj = password.[j-1] = c in
    (ci && not cj) || (cj && not ci)

let rec compte_l l valid =
    match l with
    | [] -> 0
    | e::q -> (if valid e then 1 else 0) + compte_l q valid

let _ =
    let ic = open_in "day2020_02.txt" in
    let ib = Scanf.Scanning.from_channel ic in
    let l = lit_tout ib in

    Printf.printf "Part1: %d\nPart 2: %d\n"
        (compte_l l valid1)
        (compte_l l valid2)
