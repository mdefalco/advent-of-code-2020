import re
r = re.compile(r'(?P<m>\d+)-(?P<M>\d+) (?P<c>[a-z]): (?P<password>[a-z]*)')

count = 0
for l in open('day2020_02.txt'):
    m, M, c, password = r.search(l).groups()
    o = password.count(c)
    if int(m) <= o <= int(M): count += 1
print(count)

count = 0
for l in open('day2020_02.txt'):
    m, M, c, password = r.search(l).groups()
    m, M = int(m), int(M)
    a, b = password[m-1], password[M-1]
    if (a == c or b == c) and a != b: count += 1
print(count)


