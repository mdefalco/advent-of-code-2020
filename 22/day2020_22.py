from collections import deque
from copy import copy

lines = iter(open('day2020_22.txt'))

p = []
for _ in range(2):
    next(lines)
    l = next(lines)
    p.append( deque() )
    try:
        while l != '\n':
            p[-1].append(int(l))
            l = next(lines)
    except StopIteration:
        pass

def combat(p1,p2):
    # print('Fight', p1, p2)
    rounds = []
    while len(p1) != 0 and len(p2) != 0:
        p = (tuple(p1),tuple(p2))
        if p in rounds: return True, p1
        rounds.append(p)
        c1, c2 = p1.popleft(), p2.popleft()
        if c1 <= len(p1) and c2 <= len(p2):
            p1c = copy(p1)
            while len(p1c) != c1: p1c.pop()
            p2c = copy(p2)
            while len(p2c) != c2: p2c.pop()
            p1wins = combat(p1c,p2c)[0]
        else:
            p1wins = c1 > c2

        if p1wins:
            p1.append(c1)
            p1.append(c2)
        else:
            p2.append(c2)
            p2.append(c1)

    if len(p2) == 0:
        return True, p1
    else:
        return False, p2

p1wins, winner = combat(p[0], p[1])
print('Part 1', sum((i+1)*c for i, c in enumerate(reversed(winner))))
