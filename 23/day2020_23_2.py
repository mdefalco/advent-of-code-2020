from collections import deque

data = '389125467'

d = list(map(lambda k: int(k)-1, data))

NMAX = 1000000

fi = [ i for i in range(NMAX) ]
f = [ i for i in range(NMAX) ]
for i, j in enumerate(d):
    fi[j] = i
    f[i] = j

N = NMAX

POS = 0
for k in range(100000):# * NMAX):
    cup = f[POS]

    VDEST = (cup - 1) % N
    while fi[VDEST] in [ (POS+k)%N for k in range(4) ]:
        VDEST = (VDEST - 1) % N
    DEST = fi[VDEST]

    next3 = [ f[(POS+k)%N] for k in range(1,4) ]
    l = f[POS:] + f[:POS]
    # print(next3, '|'.join( str(k + 1) if k != f[POS] else '(%d)' % (k+1) for k in l ))
    TO_MOVE = POS
    while TO_MOVE != DEST:
        v = f[TO_MOVE]
        f[(TO_MOVE + 3) % N] = v
        fi[v] = (TO_MOVE + 3) % N
        TO_MOVE = (TO_MOVE - 1) % N
    for k in range(3):
        f[(DEST+1+k)%N] = next3[k]
        fi[next3[k]] = (DEST+1+k)%N

    POS = (POS + 4) % N


print(f[fi[0]:fi[0]+3])


if False:
    for k in range(100):
        cup = l[0]
        picked = l[1:4]
        l = l[4:]
        destination = cup - 1
        if destination < m: destination = M
        while destination in [ cup ] + picked:
            destination -= 1
            if destination < m: destination = M

        i = l.index(destination)
        l = l[:i+1]+picked+l[i+1:]

        l.append(cup)
        
    while l[0] != 1:
        l = l[1:] + [l[0]]
    l = l[1:]
    print('Part 1', ''.join(map(str,l)))

