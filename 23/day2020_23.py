from collections import deque

data = '389125467'

l = list(map(int, data))
M, m = max(l), min(l)

for k in range(100):
    cup = l[0]
    picked = l[1:4]
    l = l[4:]
    destination = cup - 1
    if destination < m: destination = M
    while destination in [ cup ] + picked:
        destination -= 1
        if destination < m: destination = M

    i = l.index(destination)
    l = l[:i+1]+picked+l[i+1:]

    l.append(cup)
    
while l[0] != 1:
    l = l[1:] + [l[0]]
l = l[1:]
print('Part 1', ''.join(map(str,l)))

l = list(map(int, data))

M, m = max(l), min(l)
tofill = 1000000

start = len(l)
for k in range(start,tofill+1):
    l.append(M+k-start+1)

M = l[-1]

for k in range(10000000):
    if k % 1000 == 0:
        print(k)
    cup = l[0]
    picked = l[1:4]
    l = l[4:]
    destination = cup - 1
    if destination < m: destination = M
    while destination in [ cup ] + picked:
        destination -= 1
        if destination < m: destination = M

    i = l.index(destination)
    l = l[:i+1]+picked+l[i+1:]

    l.append(cup)

i = l.index(1)
print(l[i+1], l[i+2], l[i+1]*l[i+2])
