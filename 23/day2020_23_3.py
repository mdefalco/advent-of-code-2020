from collections import deque

data = '198753462'

class Node:
    def __init__(self, value, left=None, right=None):
        self.left = left if left is not None else self
        self.right = right if right is not None else right
        self.value = value

    def __str__(self):
        l = [ self.value ]
        loop = self.right
        while loop != self:
            l.append( loop.value )
            loop = loop.right
        return '|'.join(map(str,l))

start = Node(int(data[0]))
prev = start

N = 10
NMAX = 1000000

values_nodes = [ None ] * (NMAX + 1)

values_nodes[start.value] = start

for v in data[1:]:
    v = int(v)
    n = Node(v, left=prev, right=start)
    prev.right = n
    start.left = n
    values_nodes[v] = n
    prev = n


prev = start.left
for v in range(N,NMAX+1):
    n = Node(v, left=prev, right=start)
    prev.right = n
    start.left = n
    values_nodes[v] = n
    prev = n

def decr(v):
    if v == 1: 
        return NMAX
    return v-1

pos = start

for k in range(10 * NMAX):
    v = pos.value
    next3 = []
    follow = pos
    first_move = follow.right
    for i in range(3):
        follow = follow.right
        next3.append(follow.value)

    dest = decr(v)
    while dest in next3:
        dest = decr(dest)
    dest_node = values_nodes[dest]

    pos.right = follow.right
    follow.right.left = pos

    follow.right = dest_node.right
    follow.right.left = follow
    first_move.left = dest_node
    dest_node.right = first_move

    pos = pos.right

n = values_nodes[1]
print(n.right.value * n.right.right.value)
