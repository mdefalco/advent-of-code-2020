# Part 1
itinerary = [ (l[0],int(l[1:])) for l in open('day2020_12.txt') ]

rotate = { 90 : lambda d: (-d[1],d[0]), 
        180: lambda d: (-d[0],-d[1]),
        270: lambda d: (d[1],-d[0]) }
instructions = {
        'R' : lambda n,x,y,d: (x,y,rotate[360-n](d)),
        'L' : lambda n,x,y,d: (x,y,rotate[n](d)),
        'N' : lambda n,x,y,d : (x,y+n,d),
        'S' : lambda n,x,y,d : (x,y-n,d),
        'E' : lambda n,x,y,d : (x+n,y,d),
        'W' : lambda n,x,y,d : (x-n,y,d),
        'F' : lambda n,x,y,d : (x+n*d[0],y+n*d[1],d)
        }

state = (0,0,(1,0))
for i, n in itinerary:
    state = instructions[i](n, *state)

x, y, d = state
print(abs(x)+abs(y))
