# Part 2
itinerary = [ (l[0],int(l[1:])) for l in open('day2020_12.txt') ]

rotate = { 90 : lambda d: (-d[1],d[0]), 
        180: lambda d: (-d[0],-d[1]),
        270: lambda d: (d[1],-d[0]) }
instructions = {
        'R' : lambda n,x,y,w: (x,y,rotate[360-n](w)),
        'L' : lambda n,x,y,w: (x,y,rotate[n](w)),
        'N' : lambda n,x,y,w : (x,y,(w[0],w[1]+n)),
        'S' : lambda n,x,y,w : (x,y,(w[0],w[1]-n)),
        'E' : lambda n,x,y,w : (x,y,(w[0]+n,w[1])),
        'W' : lambda n,x,y,w : (x,y,(w[0]-n,w[1])),
        'F' : lambda n,x,y,w : (x+n*w[0],y+n*w[1],w)
        }


state = (0,0,(10,1))
for i, n in itinerary:
    state = instructions[i](n, *state)

x, y, d = state
print(abs(x)+abs(y))
