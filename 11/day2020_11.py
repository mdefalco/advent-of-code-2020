from copy import deepcopy
from itertools import product

initial_layout = [ list(l.strip()) for l in open('day2020_11.txt') ]

w, h = len(initial_layout), len(initial_layout[0])

layout = deepcopy(initial_layout)

def part1(i,j):
    neighbours = [ (i+di,j+dj) for (di,dj) in product([-1,0,1],repeat=2) \
            if (di,dj) != (0,0) and 0 <= i+di < w and 0 <= j+dj < h ]
    pattern =  ''.join([layout[vi][vj] for (vi,vj) in neighbours])
    occupied = pattern.count('#')
    return occupied

def part2(i,j):
    deltas = list(product([-1,0,1],repeat=2))
    deltas.remove((0,0))
    occupied = 0
    for di,dj in deltas:
        n = 1
        l = []
        while 0 <= i+n*di < w and 0 <= j+n*dj < h:
            v = layout[i+n*di][j+n*dj]
            l.append((i+n*di,j+n*dj,v) )
            if v == '#':
                occupied += 1
            if v != '.':
                break
            n += 1
    return occupied
            
changed = True
n = 0
part_f, part_v = part2, 5
while changed:
    n += 1
    changed = False
    new_layout = deepcopy(layout)
    for i, l in enumerate(layout):
        for j, s in enumerate(l):
            occupied = part_f(i,j)
            if s == 'L' and occupied == 0:
                new_layout[i][j] = '#'
                changed = True
            if s == '#' and occupied >= part_v:
                new_layout[i][j] = 'L'
                changed = True
    layout = new_layout

print(''.join([ ''.join(l) for l in layout  ]).count('#'))

