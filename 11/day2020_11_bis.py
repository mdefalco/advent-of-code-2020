from copy import deepcopy
from itertools import product

initial_layout = [ list(l.strip()) for l in open('day2020_11.txt') ]
w, h = len(initial_layout), len(initial_layout[0])

def neighbouring_part1(layout):
    return [ [ [ (i+di,j+dj) for (di,dj) in product([-1,0,1],repeat=2) \
                if (di,dj) != (0,0) and 0 <= i+di < w and 0 <= j+dj < h ] \
                for j in range(h) ] for i in range(w) ]

def neighbouring_part2(layout):
    deltas = list(product([-1,0,1],repeat=2))
    deltas.remove((0,0))

    neighbours = []
    def neighbouring(i, j):
        l = []
        for di,dj in deltas:
            n = 1
            while 0 <= i+n*di < w and 0 <= j+n*dj < h:
                v = layout[i+n*di][j+n*dj]
                if v != '.':
                    l.append( (i+n*di, j+n*dj) )
                    break
                n += 1
        return l

    return [ [ neighbouring(i, j) for j in range(h) ] for i in range(w) ]

layout = deepcopy(initial_layout)

neighbours = neighbouring_part2(layout)
free_threshold = 5

changed = True
while changed:
    changed = False
    new_layout = deepcopy(layout)
    for i, l in enumerate(layout):
        for j, s in enumerate(l):
            pattern =  ''.join([layout[vi][vj] for (vi,vj) in neighbours[i][j]])
            occupied = pattern.count('#')
            if s == 'L' and occupied == 0:
                new_layout[i][j] = '#'
                changed = True
            if s == '#' and occupied >= free_threshold:
                new_layout[i][j] = 'L'
                changed = True
    layout = new_layout

print(''.join([ ''.join(l) for l in layout  ]).count('#'))

