from itertools import product
from collections import defaultdict

flat_state = '''....###.
#...####
##.#.###
..#.#...
##.#.#.#
#.######
..#..#.#
######.#'''.split('\n')

dim_range = lambda dim: product(*(range(*d) for d in dim))
TOTAL_DIM = 4
dim = [ (0,len(flat_state[0])), (0,len(flat_state)) ] + [ (0,1) ] * (TOTAL_DIM - 2)
state = defaultdict(lambda : '.',
        { t : flat_state[t[1]][t[0]] for t in dim_range(dim) })

def extend(v,dv):
    return tuple( a+b for a,b in zip(v,dv) )

def update(dim, state):
    new_dim = tuple(map(lambda v: (v[0]-1,v[1]+1), dim))
    new_state = defaultdict(lambda : '.')
    for v in dim_range(new_dim):
        neighbours = [ state[extend(v,dv)] 
                for dv in product([-1,0,1],repeat=TOTAL_DIM)
                if any(x != 0 for x in  dv) ]
        active = neighbours.count('#')
        if state[v] == '.' and active == 3:
            new_state[v] = '#'
        if state[v] == '#' and 2 <= active <= 3:
            new_state[v] = '#'
    return new_dim, new_state

for _ in range(6):
    dim, state = update(dim, state)

print(sum(1 for v in dim_range(dim) if state[v] == '#'))
