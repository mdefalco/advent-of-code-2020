from itertools import product
from collections import defaultdict

flat_state = '''....###.
#...####
##.#.###
..#.#...
##.#.#.#
#.######
..#..#.#
######.#'''.split('\n')

dim_range = lambda dim: product(*(range(*d) for d in dim))
dim = [ (0,len(flat_state[0])), (0,len(flat_state)), (0,1) ]
state = defaultdict(lambda : '.',
        { (i,j,k) : flat_state[j][i] for (i,j,k) in dim_range(dim) })

def extend(v,dv):
    return tuple( a+b for a,b in zip(v,dv) )

def update(dim, state):
    new_dim = tuple(map(lambda v: (v[0]-1,v[1]+1), dim))
    new_state = defaultdict(lambda : '.')
    for v in dim_range(new_dim):
        neighbours = [ state[extend(v,dv)] 
                for dv in product([-1,0,1],repeat=3)
                if dv != (0,0,0) ]
        active = neighbours.count('#')
        if state[v] == '.' and active == 3:
            new_state[v] = '#'
        if state[v] == '#' and 2 <= active <= 3:
            new_state[v] = '#'
    return new_dim, new_state

def pprint(dim, state):
    x, y, z = dim
    for k in range(*z):
        print('Z = %d' % k)
        for j in range(*y):
            print(''.join(state[(i,j,k)] for i in range(*x)))
        print()

for _ in range(6):
    dim, state = update(dim, state)

print(sum(1 for v in dim_range(dim) if state[v] == '#'))
