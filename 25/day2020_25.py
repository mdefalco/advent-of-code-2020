
# card_pk = 5764801
# door_pk = 17807724

card_pk = 16915772
door_pk = 18447943

def transform(subject, loop_size):
    v = 1
    for _ in range(loop_size):
        v = (v * subject) % 20201227
    return v

def transform_search(subject, target):
    v = 1
    loop_size = 0
    while v != target:
        loop_size += 1
        v = (v * subject) % 20201227
    return loop_size


card_ls = transform_search(7, card_pk)
door_ls = transform_search(7, door_pk)

handshake = transform(card_pk, door_ls)
print(handshake)
