
def parse_rule(l):
    l = l.strip()
    l = l[:-1]
    l = l.replace(' bags','').replace(' bag','')
    current, others = l.split(' contain ')
    if others == 'no other':
        return (current, [])
    couples = []
    for v in others.split(', '):
        tok = v.split()
        n = int(tok[0])
        b = ' '.join(tok[1:])
        couples.append( (n,b) )
    return (current, couples)

bags = [ parse_rule(l) for l in open('day2020_07.txt') ]
bags = { bag: contents for (bag,contents) in bags }

start = 'shiny gold'
cache = {}

def can_hold(b):
    if b in cache:
        return cache[b]
    v = False
    for n, ob in bags[b]:
        if start == ob:
            v = True
            break
        v = can_hold(ob)
        if v: break
    cache[b] = v
    return v

print('Part 1', sum(1 for b in bags if can_hold(b)))


start = 'shiny gold'
cache = {}

def count(b):
    if b in cache:
        return cache[b]
    v = 0
    for n, ob in bags[b]:
        v += n * (1 + count(ob))
    cache[b] = v
    return v

for b in bags:
    count(b)

print('Part 2', count(start))
