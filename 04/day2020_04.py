l = iter(open('day2020_04.txt'))

groups = []

group = []
try:
    while True:
        v = next(l).strip()
        if v == '':
            groups.append(group)
            group = []
        else:
            group += v.split()
except StopIteration:
    groups.append(group)

passports = []
for group in groups:
    passport = {}
    for v in group:
        tok = v.split(':')
        passport[tok[0]] = tok[1]
    passports.append(passport)


valid = 0
for p in passports:
    try:
        if 'byr' not in p or len(p['byr']) != 4 or not (1920 <= int(p['byr']) <= 2002):
            continue
        if 'iyr' not in p or len(p['iyr']) != 4 or not (2010 <= int(p['iyr']) <= 2020):
            continue
        if 'eyr' not in p or len(p['eyr']) != 4 or not (2020 <= int(p['eyr']) <= 2030):
            continue

        if 'hgt' not in p or len(p['hgt']) < 2 or p['hgt'][-2:] not in ['cm','in']:
            continue

        if p['hgt'][-2:] == 'cm' and not (150 <= int(p['hgt'][:-2]) <= 193):
            continue
        if p['hgt'][-2:] == 'in' and not (59 <= int(p['hgt'][:-2]) <= 76):
            continue

        if 'hcl' not in p or len(p['hcl']) != 7 or p['hcl'][0] != '#' or not all(v in '0123456789abcdef' for v in p['hcl'][1:]):
            continue

        if 'ecl' not in p or p['ecl'] not in ['amb','blu','brn','gry','grn','hzl','oth']:
            continue

        if 'pid' not in p or len(p['pid']) != 9 or not all(v in '0123456789' for v in p['pid']):
            continue

        valid += 1
    except ValueError:
        continue

print('Part 2', valid)
