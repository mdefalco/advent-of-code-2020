
l1, l2 = open('day2020_13.txt').readlines()
timestamp, schedule = int(l1), [ int(bid) if bid != 'x' else None for bid in l2.split(',') ]

bus_ids = [ bid for bid in schedule if bid is not None ]

found_bus = None
wait = -1
while found_bus is None:
    wait += 1
    for b in bus_ids:
        if (wait + timestamp) % b == 0:
            found_bus = b
            break

print('Part 1', found_bus  * wait)

import math 

def ex_euclide(a,b):
    u, v, up, vp = 0, 1, 1, 0
    while b != 0:
        q, a, b = a // b, b, a % b
        u, v, up, vp = up-q*u, vp-q*v, u, v
    return (a, up, vp)

timestamp = 0
for b in bus_ids:
    pb = math.prod(ob for ob in bus_ids if ob != b)
    d, u, v = ex_euclide(b, pb)
    inv = v * pb
    timestamp -= schedule.index(b) * inv
print('Part 2', timestamp % math.prod(bus_ids))
